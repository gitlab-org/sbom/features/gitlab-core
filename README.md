# Gitlab-Core SBOM

Browse [available SBOMs](../-/packages?orderBy=version&sort=desc)

See https://gitlab.com/gitlab-org/sbom/ for more details.
