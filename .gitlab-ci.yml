stages:
  - generation
  - wrap
  - test
  - package

include:
  # see https://gitlab.com/components/sbom/generator/-/issues/23
  - component: gitlab.com/gitlab-org/sbom/licenses/add_missing_licenses@main
  - component: gitlab.com/components/sbom/generator/generate@~latest
    inputs:
      system_name: "gitlab-core"
      project_path: "gitlab-org/gitlab"
      dependency_scanning_max_depth: 3
  - component: gitlab.com/components/sbom/generator/wrap@~latest
    inputs:
      system_name: "gitlab-core"
  - component: gitlab.com/components/sbom/generator/validate@~latest
  - component: gitlab.com/components/sbom/generator/package@~latest


# bypass get_latest_version_from to keep only -ee versions.
# We need to rely on the latest omnibus manifests generated because we parse it later in the
# pipeline.
get-latest-version:
  stage: .pre
  image:
    name: alpine:3
    entrypoint: [""]
  script:
    - apk add curl
    - |
      export REF_NAME=$(curl --silent --show-error \
        --location \
        --fail \
        https://gitlab-org.gitlab.io/omnibus-gitlab/gitlab-manifests/manifests.html |
        sed -n "s/.*<a href='gitlab-ee\/.*'>\(.*\)\.0-ee.*/v\1/p" | sed -n '1p')
    - echo "REF_NAME=$REF_NAME" | tee sbom.env
  artifacts:
    reports:
      dotenv: sbom.env
  rules:
    - if: $REF_NAME
      when: never
    - when: on_success

generate sbom:
  variables:
    # When running gemnasium again, ignore the paths we also clean-up with the after_script
    # Default is "spec, test, tests, tmp" so we add to this list to keep the same behaviour
    DS_EXCLUDED_PATHS: "spec, test, tests, tmp, ee/spec, qa, storybook"
  after_script:
    # Clean up Gitlab SBOM
    - rm -rf ./sbom/gitlab-core/gitlab-org/gitlab/{ee,}/spec
    # If ee is still empty, remove it altogether
    - rmdir ./sbom/gitlab-core/gitlab-org/gitlab/ee || true
    - rm -rf ./sbom/gitlab-core/gitlab-org/gitlab/{qa,storybook}

# This converts the GitLab Omnibus manifest to CycloneDX
omnibus:
  extends: .generator
  script:
    - source ./omnibus.sh
    - download_omnibus_manifest "$REF_NAME"
    - convert_manifest_to_cdx "$(omnibus_manifest_path "$REF_NAME")"

# Add extra projects to the SBOM:
gitlab-shell:
  extends: .generator
  needs: ['omnibus']
  variables:
    OMNIBUS_SBOM_PATH: "sbom/gitlab-core/gitlab-org/omnibus-gitlab/manifest.cdx.json"
    PROJECT_PATH: "gitlab-org/gitlab-shell"
  before_script:
    - source /scripts/common.sh
    - export REF_NAME="$(get_version_from_sbom "gitlab-shell" "$OMNIBUS_SBOM_PATH")"

gitaly:
  extends: .generator
  needs: ['omnibus']
  variables:
    OMNIBUS_SBOM_PATH: "sbom/gitlab-core/gitlab-org/omnibus-gitlab/manifest.cdx.json"
    PROJECT_PATH: "gitlab-org/gitaly"
  before_script:
    - source /scripts/common.sh
    - export REF_NAME="$(get_version_from_sbom "gitaly" "$OMNIBUS_SBOM_PATH")"
